import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  nombre='sin nombre';
  direccion = '';
 private _dataSource = new BehaviorSubject<string>('');
 //asObservable convierte a un observable
 //dataSource$ es una variable para suscribirse a la informacion
 dataSource$ = this._dataSource.asObservable();

  constructor() { }

  public ModificarDireccion(_direccion: string): void{
     this.direccion = _direccion;
       //Con next se coloca un valor al _dataSource
    this._dataSource.next(this.direccion);
    }

}
