import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {
  mensaje = 'mensaje desde el descendiente'
  constructor() { }

  ngOnInit(): void {
  }

}
