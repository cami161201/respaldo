import { Component, OnInit, ViewChild,AfterViewInit, ChangeDetectorRef} from '@angular/core';
import { HijoComponent } from '../hijo/hijo.component';
@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements AfterViewInit {
  mensajeP!:string;

  @ViewChild(HijoComponent) hijo:any;
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.mensajeP = this.hijo.mensajeH;
    console.log(this.mensajeP);
    this.cdr.detectChanges();
   } 
  
}

